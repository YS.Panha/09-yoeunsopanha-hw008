
import React, { Component } from 'react';
import { Container, Row  } from 'react-bootstrap';
import MyForm from './components/MyForm';
import Table from './components/Table';
import NavMenu from "./components/NavMenu";

class App2 extends Component {
    render() {
        return (
            <Container>
                <NavMenu />
                <Row>
                    <MyForm />
                </Row>
                <Row>
                    <Table/>
                </Row>
            </Container>
        );
    }
}

export default App2;
