import React, { StrictMode } from "react";
import ReactDOM from "react-dom";
import App from "./App";
import App2 from "./App2";

ReactDOM.render(
  <StrictMode>
    {/* <App /> */}
    <App2/>
  </StrictMode>,
  document.getElementById("root")
);
